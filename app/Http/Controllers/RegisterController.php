<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function home()
    {
        return  "haloo";
    }
    public function home_post(Request $request)
    {
        // dd($request->all());
        $nama = $request["nama"];
        $nama_belakang = $request["nama_belakang"];
        return  view('home', compact('nama', 'nama_belakang'));
    }
}
